<?php

use App\Http\Controllers\MailTestingController;
use Illuminate\Support\Facades\Route;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/email', function () {
    
//    // Mail::to('email@email.com')->send(new WelcomeMail());

//     return new WelcomeMail();
// });

Route::view('email','userForm');
Route::post('email',[MailTestingController::class,'mailSending'])->name('mailSender');

