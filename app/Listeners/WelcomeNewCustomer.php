<?php

namespace App\Listeners;

use App\Mail\WelcomeMail;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class WelcomeNewCustomer implements ShouldQueue
{

    public function handle($event)
    {
        sleep(10);
       
        $user = new User();
        $user->name=$event->user['customar_name'];
        $user->email = $event->user['email'];
        $user->password = $event->user['password'];
        $user->save();
        Mail::to($user->email)->send(new WelcomeMail());
        // dump($user->password = $event->user['password']);
    }
}
