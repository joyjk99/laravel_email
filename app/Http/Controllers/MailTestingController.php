<?php

namespace App\Http\Controllers;

use App\Events\NewCustomerHasRegisteredEvent;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class MailTestingController extends Controller
{
    public function mailSending(Request $request)
    {
        $user = $request->all();
        event(new NewCustomerHasRegisteredEvent($user));
        
         return "Mail send";


    }
}
